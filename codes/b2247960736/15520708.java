/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
 for (int i = 0; i < n - 1; i++) { // 外层循环控制需要遍历的轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每轮中相邻元素的比较  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
