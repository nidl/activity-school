/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较和交换，将最大（或最小）的元素冒泡到数组末尾。  
 * 重复执行，直到整个数组有序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序趟数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 相邻元素两两比较，如果前一个元素比后一个元素大则交换它们  
                // 交换 a[j] 和 a[j + 1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end bubbleSort
