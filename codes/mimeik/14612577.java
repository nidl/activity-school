/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组  1111
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    
    for (int i = 0; i < n; i++) {
    	for (int j = 0; j < i; j++) {
		if (a[i] <  a[j]) {
		 	int t = a[i];
			a[i] = a[j];
			a[j] = t;
		}
	}
    }
}//end
