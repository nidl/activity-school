/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
	if(a != null && a.length > 0){
		int l = a.length;
		if(l > n){
			l = n;
		}
		for (int i = 0; i< l; i++){
			for (int j=0; j<l-i-1; j++){
				if(a[j] > a[j+1]){
					int t = a[j];
					a[j] = a[j+1];
					a[j+1] = t;
				}
			}
		}
	}
} //end